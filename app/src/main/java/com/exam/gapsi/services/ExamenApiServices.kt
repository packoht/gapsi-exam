package com.exam.gapsi.services

import com.exam.gapsi.flow.models.ProductsResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface ExamenApiServices {

    @Headers("X-IBM-Client-Id: adb8204d-d574-4394-8c1a-53226a40876e")
    @GET("search")
    fun searchProduct(
        @Query("query") product: String
    ): Single<ProductsResponse>
}