package com.exam.gapsi.flow.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.exam.gapsi.base.BaseViewModel
import com.exam.gapsi.flow.models.ItemProduct
import com.exam.gapsi.flow.models.ProductsResponse
import com.exam.gapsi.repository.SearchUserRepository
import com.exam.gapsi.scopes.ActivityScope
import javax.inject.Inject

@ActivityScope
class SearchProductViewModel @Inject constructor(
    private var searchProductRepository: SearchUserRepository
) : BaseViewModel() {

    private val products = MutableLiveData<List<ItemProduct>>()
    fun getProducts(): LiveData<List<ItemProduct>> = products

    fun searchProduct(product: String) {
        disposable.add(
            searchProductRepository.searchProduct(product)
                .doOnSubscribe { showProgress.value = true }
                .doFinally { showProgress.value = false }
                .subscribe({
                    products.value = it.items
                }, {
                    showErrorMessage.value = it.localizedMessage
                })
        )
    }
}