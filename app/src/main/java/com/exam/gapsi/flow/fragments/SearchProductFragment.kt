package com.exam.gapsi.flow.fragments

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.SearchView
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.exam.gapsi.base.BaseFragment
import com.exam.gapsi.databinding.FragmentSearchProductBinding
import com.exam.gapsi.extensions.show
import com.exam.gapsi.flow.models.ItemProduct
import com.exam.gapsi.flow.models.ProductsResponse
import com.exam.gapsi.flow.viewModel.SearchProductViewModel
import com.exam.gapsi.flow.views.ItemProductView
import com.xwray.groupie.GroupAdapter
import com.xwray.groupie.GroupieViewHolder
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

class SearchProductFragment : BaseFragment() {

    @Inject
    lateinit var viewModel: SearchProductViewModel

    private lateinit var binding: FragmentSearchProductBinding

    private val groupAdapter = GroupAdapter<GroupieViewHolder>()

    override fun onAttach(context: Context) {
        super.onAttach(context)
        AndroidSupportInjection.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentSearchProductBinding.inflate(inflater)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        bindViewModel()
        initUi()
        initRecyclerView()
    }

    private fun bindViewModel() {
        viewModel.getShowProgress().observe(viewLifecycleOwner, Observer(::showLoading))
        viewModel.getShowErrorMessage().observe(viewLifecycleOwner, Observer (::showError))
        viewModel.getProducts().observe(viewLifecycleOwner, Observer (::showAdapter))
    }

    private fun showAdapter(productsList: List<ItemProduct>) {
        groupAdapter.addAll(
            productsList.map {
                ItemProductView(it, requireContext())
            }
        )
    }

    private fun initRecyclerView() {
        binding.recyclerViewList.apply {
            layoutManager = LinearLayoutManager(context)
            adapter = groupAdapter
        }
    }

    private fun initUi() {
        binding.buttonSearch.setOnClickListener {
            viewModel.searchProduct(binding.searchViewProducts.query.toString())
        }
        binding.searchViewProducts.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(p0: String): Boolean = false

            override fun onQueryTextChange(p0: String?): Boolean {
                binding.buttonSearch.show()
                if (p0!!.isEmpty()) {
                    groupAdapter.clear()
                }
                return true
            }
        })
    }
}