package com.exam.gapsi.flow.module

import com.exam.gapsi.flow.fragments.SearchProductFragment
import com.exam.gapsi.scopes.FragmentScope
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class SearchProductModule {

    @FragmentScope
    @ContributesAndroidInjector
    abstract fun contributesSearchProductFragment(): SearchProductFragment
}