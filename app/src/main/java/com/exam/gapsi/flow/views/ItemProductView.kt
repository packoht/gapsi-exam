package com.exam.gapsi.flow.views

import android.content.Context
import android.view.View
import com.exam.gapsi.R
import com.exam.gapsi.databinding.ViewItemProductBinding
import com.exam.gapsi.flow.models.ItemProduct
import com.squareup.picasso.Picasso
import com.xwray.groupie.viewbinding.BindableItem

class ItemProductView(
    private val itemProduct: ItemProduct,
    private val context: Context
):BindableItem<ViewItemProductBinding>() {


    override fun getLayout(): Int = R.layout.view_item_product

    override fun bind(viewBinding: ViewItemProductBinding, position: Int) {
        val rating = String.format("%.2f", itemProduct.itemRating)
        val amount = String.format("%.2f", itemProduct.itemPrice)
        viewBinding.run {
            textViewCardTitle.text = itemProduct.itemTitle
            textViewCardRating.text = context.getString(R.string.products_search_rating, rating)
            textViewCardAmount.text = context.getString(R.string.products_search_amount, amount)
        }

        Picasso.get()
            .load(itemProduct.itemImage)
            .into(viewBinding.imageViewProduct)
    }

    override fun initializeViewBinding(view: View): ViewItemProductBinding = ViewItemProductBinding.bind(view)
}