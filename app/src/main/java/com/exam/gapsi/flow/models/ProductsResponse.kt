package com.exam.gapsi.flow.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ProductsResponse(
    @SerializedName("totalResults") val totalResult: Double,
    @SerializedName("page") val page: Int,
    @SerializedName("items") val items: List<ItemProduct>
): Parcelable

@Parcelize
data class ItemProduct(
    @SerializedName("id") val itemId: String,
    @SerializedName("rating") val itemRating: Float,
    @SerializedName("price") val itemPrice: Float,
    @SerializedName("image") val itemImage: String,
    @SerializedName("title") val itemTitle: String
): Parcelable