package com.exam.gapsi.di.module

import com.exam.gapsi.di.rest.ExamApi
import com.exam.gapsi.di.rest.Microservices
import com.exam.gapsi.services.ExamenApiServices
import dagger.Module
import dagger.Provides
import dagger.Reusable
import retrofit2.Retrofit

@Module
class ExamServicesApiModule {

    @ExamApi
    @Provides
    @Reusable
    fun provideUserApiService(@Microservices retrofit: Retrofit): ExamenApiServices {
        return retrofit.create(ExamenApiServices::class.java)
    }
}