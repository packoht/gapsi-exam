package com.exam.gapsi.di.module

import com.exam.gapsi.di.rest.Microservices
import com.exam.gapsi.network.retrofitBuilder
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import javax.inject.Singleton

private const val BASE_URL_SERVICES = "https://00672285.us-south.apigw.appdomain.cloud/demo-gapsi/"

@Module(
    includes = [
        ExamServicesApiModule::class
    ]
)
object ExamenServicesModule {

    @Provides
    @Singleton
    @Microservices
    @JvmStatic
    fun microServicesHttpClient(
        httpClient: OkHttpClient
    ): Retrofit {
        return retrofitBuilder(
            httpClient,
            BASE_URL_SERVICES
        ).build()
    }
}