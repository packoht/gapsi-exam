package com.exam.gapsi.di.module

import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import dagger.Reusable

@Module(
    includes = [
        ExamenServicesModule::class
    ]
)
class ExamModule {

    @Provides
    @Reusable
    fun provideGson(): Gson = Gson()
}