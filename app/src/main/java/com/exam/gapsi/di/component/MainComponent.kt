package com.exam.gapsi.di.component

import com.exam.gapsi.application.InjectableApplication
import com.exam.gapsi.di.module.ExamAppModule
import com.exam.gapsi.di.module.ExamModule
import com.exam.gapsi.di.module.ExamenActivityBuilder
import com.exam.gapsi.di.module.NetworkModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        ExamenActivityBuilder::class,
        ExamAppModule::class,
        NetworkModule::class,
        ExamModule::class,
        AndroidSupportInjectionModule::class
    ]
)
interface MainComponent : BaseComponent {

    @Component.Builder
    interface Builder {
        fun build(): MainComponent

        @BindsInstance
        fun application(application: InjectableApplication): Builder
    }
}