package com.exam.gapsi.di.component

import android.app.Application
import android.content.Context
import com.exam.gapsi.application.InjectableApplication
import com.exam.gapsi.di.rest.Microservices
import com.google.gson.Gson
import dagger.android.AndroidInjector
import retrofit2.Retrofit

interface BaseComponent : AndroidInjector<InjectableApplication> {

    fun context(): Context

    fun application(): Application

    fun gson(): Gson

    @Microservices
    fun retrofitMicroService(): Retrofit
}