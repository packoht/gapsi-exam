package com.exam.gapsi.di.rest

import javax.inject.Qualifier

@Qualifier
annotation class Microservices

@Qualifier
annotation class AuthorizationInterceptors

@Qualifier
annotation class ExamApi