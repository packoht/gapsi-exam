package com.exam.gapsi.di.module

import com.exam.gapsi.flow.SearchProductActivity
import com.exam.gapsi.flow.module.SearchProductModule
import com.exam.gapsi.scopes.ActivityScope
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ExamenActivityBuilder {

    @ActivityScope
    @ContributesAndroidInjector(
        modules = [
            SearchProductModule::class
        ]
    )
    abstract fun bindSearchProductActivity(): SearchProductActivity
}