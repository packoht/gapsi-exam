package com.exam.gapsi.repository

import com.exam.gapsi.di.rest.ExamApi
import com.exam.gapsi.extensions.applySchedulers
import com.exam.gapsi.flow.models.ProductsResponse
import com.exam.gapsi.services.ExamenApiServices
import dagger.Reusable
import io.reactivex.Single
import javax.inject.Inject

@Reusable
class SearchUserRepository @Inject constructor(
    @ExamApi private val apiService: ExamenApiServices
) {

    fun searchProduct(product: String): Single<ProductsResponse> {
        return apiService.searchProduct(product).applySchedulers()
    }
}